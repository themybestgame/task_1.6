using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moving : MonoBehaviour
{
    private void Start()
    {
        Debug.Log("Начинаю задание!");
    }

    void Update()
    {
        Move();
    }

    private void Move()
    {
        Vector3 speed = new Vector3(1, 0, 0);
        Vector3 direction = speed * Time.deltaTime;
        transform.position += direction;
    }
}
