using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosive : MonoBehaviour
{
    public Rigidbody2D[] bombParts;
    [SerializeField] float impulse;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        gameObject.SetActive(false);
        Debug.Log("Внимание! Взрыв!");
        Boom();
    }

    void Boom()
    {
        foreach(var bombParts in bombParts)
        {
            Vector2 dir = gameObject.transform.position;
            dir.x = Random.Range(-impulse, impulse);
            dir.y = Random.Range(-impulse, impulse);
            bombParts.AddForce(dir, ForceMode2D.Impulse);
        }
    }
}
